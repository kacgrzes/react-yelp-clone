const webpack = require('webpack');
const fs = require('fs');
const path = require('path'),
    join = path.join,
    resolve = path.resolve;

const getConfig = require('hjs-webpack');

const NODE_ENV = proces.env.NODE_ENV;
const isev = NODE_ENV === 'development';

var config = getConfig({
    isDev: isDev,
    in: join(src, 'app.js'),
    out: dest,
    clearBeforeBuild: true
});

module.exports = config;
